﻿using UnityEngine;

public class OffensiveMove
{

    private BoxCollider2D collisionBox;

    public BoxCollider2D CollisionBox
    {
        get
        {
            return collisionBox;
        }
        set
        {
            collisionBox = value;
        }
    }

    private int startUp;

    public int StartUp
    {
        get
        {
            return startUp;
        }
        set
        {
            startUp = value;
        }
    }

    private int active;

    public int Active
    {
        get
        {
            return active;
        }
        set
        {
            active = value;
        }
    }

    private int recovery;

    public int Recovery
    {
        get
        {
            return recovery;
        }
        set
        {
            recovery = value;
        }
    }
        
    private int damage;

    public int Damage
    {
        get
        {
            return damage;
        }
        set
        {
            damage = value;
        }
    }

    private int stun;

    public int Stun
    {
        get
        {
            return stun;
        }
        set
        {
            stun = value;
        }
    }
        
    public OffensiveMove(BoxCollider2D collisionBox, int startUp, int active, int recovery, int damage, int stun)
    {
        this.CollisionBox = collisionBox;
        this.StartUp = startUp;
        this.Active = active;
        this.Recovery = recovery;
        this.Damage = damage;
        this.Stun = stun;
    }   
}