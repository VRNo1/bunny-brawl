﻿using UnityEngine;
using System.Collections.Generic;

public class CameraHandler : MonoBehaviour
{

    //Camera variables
    public float CameraSpeed = 7f;
    public float ClosestZoom = 2.5f;
    public float CameraRise = 2f;
    List<GameObject> players = new List<GameObject>();
    
    // Use this for initialization
    void Start()
    {
        foreach (GameObject obj in GameObject.FindGameObjectsWithTag("Player"))
        {
            players.Add(obj);
        }
    }
    
    // Update is called once per frame
    void Update()
    {

        //  ChangeTime();
        
        float xMin = float.PositiveInfinity, xMax = float.NegativeInfinity;
        float yMin = float.PositiveInfinity, yMax = float.NegativeInfinity;
            
        foreach (GameObject player in players)
        {
            xMin = Mathf.Min(xMin, player.transform.position.x);
            xMax = Mathf.Max(xMax, player.transform.position.x);
            
            yMin = Mathf.Min(yMin, player.transform.position.y + CameraRise);
            yMax = Mathf.Max(yMax, player.transform.position.y + CameraRise);
        
        }
            
                
        //Adjust size/zoom
        float[] camSizeValues = {(xMax - xMin) / 2, (yMax - yMin) / 2, ClosestZoom};
                
        //Debug.Log(string.Format("{0},{1},{2},{3}", xMax,xMin,yMax,yMin));
        //Debug.Log (string.Format ("{0},{1},{2}", camSizeValues [0], camSizeValues [1], camSizeValues [2]));
                
        if (camera.orthographic)
            camera.orthographicSize = Mathf.Lerp(camera.orthographicSize, Mathf.Max(camSizeValues), Time.deltaTime * CameraSpeed);

        //Adjust position
        Vector3 centerPoint;
        if (camera.orthographic)
        {
            centerPoint = new Vector3(xMax - ((xMax - xMin) / 2.0f), yMax - ((yMax - yMin) / 2.0f), transform.position.z);
        } else
        {
            centerPoint = new Vector3(xMax - ((xMax - xMin) / 2.0f), yMax - ((yMax - yMin) / 2.0f), -(Mathf.Max(camSizeValues) + 2 * CameraRise));
        }
                
        camera.transform.position = Vector3.Lerp(camera.transform.position, centerPoint, Time.deltaTime * CameraSpeed);     
    }

    void ChangeTime()
    {
        if (Input.GetKeyDown(KeyCode.KeypadPlus))
        {
            Debug.LogWarning(Time.timeScale);
            Time.timeScale += 0.2f;
        }
        if (Input.GetKeyDown(KeyCode.KeypadMinus))
        {
            Time.timeScale -= 0.2f;
        }
    }
}