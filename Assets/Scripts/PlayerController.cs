using System;
using TPInputLibrary;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public enum StateEnum
    {
        Idle,
        Walking,
        Running,
        Airbourne,
        Stunned, 
        AirStunned,
        Block,
        AirBlock,
        Hurt,
        AirHurt,
        KnockedOut,
        PunchForward,
        PunchUp,
        PunchDown
    }
        
    //GameObjectHelpers
    [SerializeField]
    public GameObject
        BunnySprite ;
    [SerializeField]
    public Transform
        BunnyFeetLocation;
        
    //Player Input details
    public int PlayerNumber;
    
    [HideInInspector]
    [SerializeField]
    private InputHandler.InputType
        inputType;
    [ExposeProperty]
    public InputHandler.InputType InputType
    {
        get
        {
            return inputType;
        }
        set
        {
            inputType = value;
            input = new InputHandler(PlayerNumber, inputType);
        }
    }    
    
    
    [HideInInspector]
    [SerializeField]
    private InputHandler
        input;
    
    //State and animator
    StateEnum CurrentState;
    AnimationController animControl;
        
    //Physical properties (movement speed, weight)
    [SerializeField]
    public float
        MovementSpeed = 20f;
    [SerializeField]
    public float
        Weight = 10f;

    void Start()
    {
        
        animControl = (AnimationController)gameObject.GetComponent("AnimationController");
        input = new InputHandler(PlayerNumber, inputType);
    }
                
    void FixedUpdate()
    {       
                
        //Calculate current state before getting inputs
        AssessCurrentState();
        
        //Retrieve and convert input
        if (input != null)
        {
            input.InputUpdate();
            InputState inputState = input.CurrentState;
                
            FlipBunny(inputState.MovementAxis.X);
                
            //Adjust bunny speed
            if (inputState.MovementAxis.XNorm > 0.3)
            {
                rigidbody2D.velocity = new Vector2(Mathf.Clamp((inputState.MovementAxis.X * MovementSpeed), -MovementSpeed, MovementSpeed), rigidbody2D.velocity.y);
            } else
            {
                rigidbody2D.velocity = new Vector2(0, rigidbody2D.velocity.y);
            }
                
            switch (CurrentState)
            {
                
            //You can jump from Idle, Walking and Running.
                case StateEnum.Idle:
                case StateEnum.Walking:
                case StateEnum.Running:
                    if (inputState.Actions.Jump)
                    {
                        rigidbody2D.velocity = (new Vector2(rigidbody2D.velocity.x, 5.0f));
                    }
                        
                    break;
            }
            if (inputState.Actions.Punch && CheckGroundedState())
             
                CurrentState = StateEnum.PunchForward;
        }    
        animControl.SendState(CurrentState);
            
    }
                
    void AssessCurrentState()
    {
        //TODO: Check if damaged - any damage affects should ovveride normal states and controls
        
        //Check if on the ground
        if (CheckGroundedState())
        {
                        
            //Set if running or idle
            //Debug.Log ("Vel:" + gameObject.rigidbody2D.velocity.x);
            float velX = rigidbody2D.velocity.x * Math.Sign(rigidbody2D.velocity.x);
            if (velX >= MovementSpeed / 2)
            {
                CurrentState = StateEnum.Running;
            } else if (velX > 0)
            {
                CurrentState = StateEnum.Walking;
            } else
            {
                CurrentState = StateEnum.Idle;
            }

        } else
        {
            CurrentState = StateEnum.Airbourne;
        }
                
        //If in the air then check 
        
    }
    
    bool CheckGroundedState()
    {
        bool grounded = Physics2D.Linecast(transform.position, BunnyFeetLocation.position, 1 << LayerMask.NameToLayer("Ground"));
        //Debug.DrawLine (transform.position, BunnyFeetLocation.position);
        return grounded;    
    }
        
    void SwingSword()
    {
        
        Debug.Log("Swing");//hitBoxes [0].tag);
    }
        
    void FlipBunny(float xAxisValue)
    {
        if (xAxisValue > 0)
        {
            BunnySprite.transform.localScale = new Vector2(1, 1);
        } else if (xAxisValue < 0)
        {
            BunnySprite.transform.localScale = new Vector2(-1, 1);
        }
    }
}


