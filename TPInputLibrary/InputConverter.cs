using System;
namespace TPInputLibrary
{
    public abstract class InputConverter
    {
        public  abstract InputState Convert(); 
    }               
}

